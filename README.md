# CraigCrawler

## Description
The CraigCrawler is a noncontiguous Craigslist web scraper written in Python. The CraigCrawler will search as many locations as you provide with the same search criteria, a feature not available on Craigslist itself.

## History
For many folks in this country, the concept of a tri-state or multi-state area is quite a regular thing. It is quite common to live in one state, purchase groceries in another and possibly purchase appliances in yet another. For folks who live in these areas, searching Craigslist noncontiguously is a small chore. Further compound that with folks who live in multiple places that are far enough away to not be considered "nearby". Enter the CraigCrawler. At the very beginning of my career, I built the CraigCrawler using only HTML, CSS and Javascript. For my limited purposese it was quite enjoyable. I thought it would be fun to rebuild this tool using Python.

## Instructions
The CraigCrawler uses the argparse module, so you can get a list of the commands by running `-h` as an argument:
```
$ ./craig-crawler.py -h
usage: craig-crawler.py [-h] [-l [LOCATION [LOCATION ...]]] [-c CATAGORY] [-s SUBCATAGORY] [-q QUERY] [-n MIN] [-x MAX] [--title] [--image] [--today] [--bundle] [--nearby] [--categories]
                        [--subcategories] [--cities]

optional arguments:
  -h, --help            show this help message and exit
  -l [LOCATION [LOCATION ...]], --location [LOCATION [LOCATION ...]]
                        The locations you want to search. Separate location subdomains by comma (i.e. sfbay,philadelphia,miami). The default is 'sfbay'.
  -c CATAGORY, --catagory CATAGORY
                        The catagory you want to search in. The default is 'for sale' or 'sss'.
  -s SUBCATAGORY, --subcatagory SUBCATAGORY
                        The subcatagory you want to search in. The default is None.
  -q QUERY, --query QUERY
                        The keyword or phrase that you are searching for.
  -n MIN, --min MIN     The minimum price to search by.
  -x MAX, --max MAX     The maximum price to search by.
  --title               Search titles only.
  --image               Must have an image.
  --today               Search only posts from today.
  --bundle              Bundle duplicate posts.
  --nearby              Include nearby areas in the search.
  --categories          List all of the possible categories available to select from.
  --subcategories       List all of the possible subcategories available to select from.
  --cities              List all of the possible cities available to select from.
```

For those who don't know the categories, sub-categores and city codes, you can pass in those flags to get a handy reference list. Note that not all of the cities that are available are included in the reference list _(because there are a LOT)_ but you can still pass those values and it will work.
```
$ ./craig-crawler.py --cities
Cities:
-=-=-=-=-=-=-=-
atlanta | Atlanta
austin | Austin
boston | Boston
chicago | Chicago
dallas | Dallas
denver | Denver
detroit | Detroit
houston | Houston
lasvegas | Las Vegas
losangeles | Los Angeles
miami | Miami
minneapolis | Minneapolis
newyork | New York
orangecounty | Orange Co
philadelphia | Philadelphia
phoenix | Phoenix
portland | Portland
raleigh | Raleigh
sacramento | Sacramento
sandiego | San Diego
seattle | Seattle
sfbay | Sf Bayarea
washingtondc | Wash Dc
```
With the way that the CraigCrawler is built, you can peform very detailed searches in a way that will allow you to cover very specific geographic locations or whole patches of the United States. Note that CraigCrawler is set to a two second sleep cycle for each location that you search. Certainly you can change that but well... see the section below "Notice of Liabilty". To show the capability of the CraigCrawler, here is an example of a search covering four major cities in California looking for a Nintendo DS Lite in the Video Gaming sub-category between $3 and $80. I am also only searching in the title for my keywords and the results must have an image included with them:

```
$ ./craig-crawler.py -l sacramento sfbay sandiego losangeles -s vga -q "nintendo ds lite" -n 3 -x 80 --title --image
{
    "sacramento": [
        "https://sacramento.craigslist.org/vgm/d/sacramento-nintendo-ds-lite-with/7273861054.html",
        "https://sacramento.craigslist.org/vgm/d/roseville-nintendo-ds-lite-charger/7264832113.html",
        "https://sfbay.craigslist.org/eby/vgm/d/san-ramon-nintendo-ds-lite-pink/7272320714.html"
    ],
    "sfbay": [
        "https://sfbay.craigslist.org/eby/vgm/d/san-ramon-nintendo-ds-lite-pink/7272320714.html",
        "https://sacramento.craigslist.org/vgm/d/sacramento-nintendo-ds-lite-with/7273861054.html",
        "https://sacramento.craigslist.org/vgm/d/roseville-nintendo-ds-lite-charger/7264832113.html"
    ],
    "sandiego": [
        "https://inlandempire.craigslist.org/vgm/d/menifee-nintendo-ds-lite/7268773528.html"
    ],
    "losangeles": [
        "https://inlandempire.craigslist.org/vgm/d/menifee-nintendo-ds-lite/7268773528.html",
        "https://ventura.craigslist.org/vgm/d/newbury-park-naki-world-nintendo-ds/7256279557.html"
    ]
}
```
For folks who are collectors and are willing to pay to ship items that might not be listed on eBay, this is a handy tool to help them find those sellers.

## Notice of Liability
The CraigCrawlwer is not a security tool. It's not meant to used for penetration testing purposes. This tool is being presented here as a handy script to help folks who want to optimize their use of Craigslist. Use of this code and this script is at your own risk and any liability for its use or misuse is the responsibility of the individual who runs it. In other words, use at your own discretion.