#!/usr/bin/python3

import time
import json
import argparse
import urllib3
import random
from bs4 import BeautifulSoup
from lib.craig_data import CraigData

class CraigCrawler:
    """ A class for noncontiguously searching Craigslist. """
    
    def __init__(self):
        self.results = {}
        self.user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) \
            AppleWebKit/537.36 (KHTML, like Gecko) \
                Chrome/74.0.3729.169 Safari/537.36"

        self.craig_data = CraigData()
        self.categories = self.craig_data.categories
        self.subcategories = self.craig_data.subcategories
        self.cities = self.craig_data.cities

        self.parser = argparse.ArgumentParser()

        self.parser.add_argument("-l", "--location", nargs="*",
            default="sfbay", help="The locations you want to search. Separate \
                location subdomains by comma (i.e. sfbay,philadelphia,miami). \
                    The default is 'sfbay'.")
        
        self.parser.add_argument("-c", "--catagory", default="sss",
            help="The catagory you want to search in. The default is \
                'for sale' or 'sss'.")
        
        self.parser.add_argument("-s", "--subcatagory", default=None,
            help="The subcatagory you want to search in. The default is None.")
        
        self.parser.add_argument("-q", "--query", default="apple",
            help="The keyword or phrase that you are searching for.")

        self.parser.add_argument("-n", "--min", default=None, help="The minimum \
            price to search by.")
        
        self.parser.add_argument("-x", "--max", default=None, help="The maximum \
            price to search by.")
        
        self.parser.add_argument("--title", default=None,
            action="store_true", help="Search titles only.")

        self.parser.add_argument("--image", default=None,
            action="store_true", help="Must have an image.")

        self.parser.add_argument("--today", default=None, action="store_true",
            help="Search only posts from today.")

        self.parser.add_argument("--bundle", default=None, action="store_true",
            help="Bundle duplicate posts.")

        self.parser.add_argument("--nearby", default=None, action="store_true",
            help="Include nearby areas in the search.")
        
        self.parser.add_argument("--categories", action="store_true",
            help="List all of the possible categories available to select \
                from.", dest="show_categories")
        
        self.parser.add_argument("--subcategories", action="store_true",
            help="List all of the possible subcategories available to select \
                from.", dest="show_subcategories")

        self.parser.add_argument("--cities", action="store_true", help="List \
            all of the possible cities available to select from.",
            dest="show_cities")
        
        self.parse_args()


    def parse_args(self):
        """This function parses the arguments that are passed in from the
            command line.

        Parameters
        ----------
        self : object
            A reference to the current instance of the class

        """
        args = self.parser.parse_args()
        base_sleep = 2
        random_sleep_seed = random.randint(0, 100)/100
        
        if args.show_categories:
            print("Catagories:\n-=-=-=-=-=-=-=-")
            self.show_values("cat")
        
        if args.show_subcategories:
            print("Sub-Categories:\n-=-=-=-=-=-=-=-")
            self.show_values("sub")

        if args.show_cities:
            print("Cities:\n-=-=-=-=-=-=-=-")
            self.show_values("city")

        category = args.subcatagory \
            if args.subcatagory is not None else args.catagory

        if args.show_categories is False and \
            args.show_subcategories is False and \
            args.show_cities is False:
            
            for loc in args.location:
                options = {
                    "location": loc,
                    "catagory": category,
                    "query": args.query,
                    "min": args.min,
                    "max": args.max,
                    "search_type": args.title,
                    "image": args.image,
                    "today": args.today,
                    "bundle": args.bundle,
                    "nearby": args.nearby
                }
                
                self.results[loc] = []
                self.request_url(options)
                time.sleep(base_sleep + random_sleep_seed)
                
            self.display_json()


    def request_url(self,options):
        """This function creates the proper url based on options provided and
            makes a GET request.

        Parameters
        ----------
        self : object
            A reference to the current instance of the class
            
        options : dictionary
            A dictionary of configurations for the creation of the URL
        """

        user_agent = {'user-agent': self.user_agent}
        http = urllib3.PoolManager(headers=user_agent)
        
        url = "https://%s.craigslist.org/search/%s?query=%s" % (
            options["location"], 
            options["catagory"], 
            options["query"]
        )

        if options.get("min") is not None:
            url += "&min_price=%s" % options.get("min")

        if options.get("max") is not None:
            url += "&max_price=%s" % options.get("max")
        
        if options.get("search_type") is not None:
            url += "&srchType=T"

        if options.get("image") is not None:
            url += "&hasPic=1"

        if options.get("today") is not None:
            url += "&postedToday=1"

        if options.get("bundle") is not None:
            url += "&bundleDuplicates=1"

        if options.get("nearby") is not None:
            url += "&searchNearby=1" 

        req = http.request('GET', url)

        html_page = BeautifulSoup(req.data, 'html.parser')
        links = html_page.find_all('a','result-image gallery')

        for link in links:
            self.results[options.get("location")].append(link.get('href'))


    def display_json(self):
        """This function parses the resulting dictionary object as JSON and
            prints it to the screen.

        Parameters
        ----------
        self : object
            A reference to the current instance of the class
            
        """
        data = json.dumps(self.results, indent = 4)   
        print(data)  


    def show_values(self, type):
        """ This function displays the various prefix lists that are needed for
            customizing the GET request.

        Parameters
        ----------
        self : object
            A reference to the current instance of the class
            
        """
        if type == "cat":
            data_set = self.categories
        elif type == "sub":
            data_set = self.subcategories
        else:
            data_set = self.cities

        for item in data_set:
            print("%s | %s" % (item["prefix"], item["label"].title()))

cc = CraigCrawler()