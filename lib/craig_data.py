#!/usr/bin/python3

class CraigData:
    def __init__(self):
        self.categories = [
            {"prefix":"ccc","label":"community"},
            {"prefix":"eee","label":"events"},
            {"prefix":"sss","label":"for sale"},
            {"prefix":"ggg","label":"gigs"},
            {"prefix":"hhh","label":"housing"},
            {"prefix":"jjj","label":"jobs"},
            {"prefix":"rrr","label":"resumes"},
            {"prefix":"bbb","label":"services"}
        ]

        self.subcategories = [
            {"prefix":"sss","label":"all"},
            {"prefix":"ata","label":"antiques"},
            {"prefix":"ppa","label":"appliances"},
            {"prefix":"ara","label":"arts+crafts"},
            {"prefix":"sna","label":"atvs/utvs/snow"},
            {"prefix":"pta","label":"auto parts"},
            {"prefix":"wta","label":"auto wheels & tires"},
            {"prefix":"ava","label":"aviation"},
            {"prefix":"baa","label":"baby+kids"},
            {"prefix":"bar","label":"barter"},
            {"prefix":"haa","label":"beauty+hlth"},
            {"prefix":"bip","label":"bike parts"},
            {"prefix":"bia","label":"bikes"},
            {"prefix":"bpa","label":"boat parts"},
            {"prefix":"boo","label":"boats"},
            {"prefix":"bka","label":"books"},
            {"prefix":"bfa","label":"business"},
            {"prefix":"cta","label":"cars+trucks"},
            {"prefix":"ema","label":"cds/dvd/vhs"},
            {"prefix":"moa","label":"cell phones"},
            {"prefix":"cla","label":"clothes+acc"},
            {"prefix":"cba","label":"collectibles"},
            {"prefix":"syp","label":"computer parts"},
            {"prefix":"sya","label":"computers"},
            {"prefix":"ela","label":"electronics"},
            {"prefix":"gra","label":"farm+garden"},
            {"prefix":"zip","label":"free stuff"},
            {"prefix":"fua","label":"furniture"},
            {"prefix":"gms","label":"garage sales"},
            {"prefix":"foa","label":"general"},
            {"prefix":"hva","label":"heavy equipment"},
            {"prefix":"hsa","label":"household"},
            {"prefix":"jwa","label":"jewelry"},
            {"prefix":"maa","label":"materials"},
            {"prefix":"mpa","label":"motorcycle parts"},
            {"prefix":"mca","label":"motorcycles"},
            {"prefix":"msa","label":"music instr"},
            {"prefix":"pha","label":"photo+video"},
            {"prefix":"rva","label":"RVs"},
            {"prefix":"sga","label":"sporting"},
            {"prefix":"tia","label":"tickets"},
            {"prefix":"tla","label":"tools"},
            {"prefix":"taa","label":"toys+games"},
            {"prefix":"tra","label":"trailers"},
            {"prefix":"vga","label":"video gaming"},
            {"prefix":"waa","label":"wanted"}
        ]

        self.cities = [
            {"prefix":"atlanta","label":"atlanta"},
            {"prefix":"austin","label":"austin"},
            {"prefix":"boston","label":"boston"},
            {"prefix":"chicago","label":"chicago"},
            {"prefix":"dallas","label":"dallas"},
            {"prefix":"denver","label":"denver"},
            {"prefix":"detroit","label":"detroit"},
            {"prefix":"houston","label":"houston"},
            {"prefix":"lasvegas","label":"las vegas"},
            {"prefix":"losangeles","label":"los angeles"},
            {"prefix":"miami","label":"miami"},
            {"prefix":"minneapolis","label":"minneapolis"},
            {"prefix":"newyork","label":"new york"},
            {"prefix":"orangecounty","label":"orange co"},
            {"prefix":"philadelphia","label":"philadelphia"},
            {"prefix":"phoenix","label":"phoenix"},
            {"prefix":"portland","label":"portland"},
            {"prefix":"raleigh","label":"raleigh"},
            {"prefix":"sacramento","label":"sacramento"},
            {"prefix":"sandiego","label":"san diego"},
            {"prefix":"seattle","label":"seattle"},
            {"prefix":"sfbay","label":"sf bayarea"},
            {"prefix":"washingtondc","label":"wash dc"}
        ]